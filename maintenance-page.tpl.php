<?php

/**
 * @file maintenance-page.tpl.php
 *
 * Theme implementation to display a single Drupal page while off-line.
 *
 * All the available variables are mirrored in page.tpl.php. Some may be left
 * blank but they are provided for consistency.
 *
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  <?php print $styles; ?>
  <?php print $scripts; ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyled Content in IE */ ?> </script>
</head>
<body class="<?php print $body_classes; ?>">
<div id="wrapper">
	<div id="shadow">
		<div id="page">
        	<div id="name-and-slogan">
				<?php if (!empty($site_name)): ?>
				<h1 id="site-name"><a href="<?php print $base_path ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a></h1>
				<?php endif; ?>

				<?php if (!empty($site_slogan)): ?>
				<div id="site-slogan"><?php print $site_slogan; ?></div><div class="hr"></div>
				<?php endif; ?>
			</div> <!-- /name-and-slogan -->
	
			<div id="content">
				<?php if (!empty($title)): ?><h3 class="storytitle" id="page-title"><?php print $title; ?></h3><?php endif; ?>
				<?php if (!empty($messages)): print $messages; endif; ?>
					<div id="content-content" class="clear-block">
					<?php print $content; ?>
					</div> <!-- /content-content -->
        	</div> <!-- /content -->
		<div class="clear"/></div></div>
	</div><div class="clear"/></div>
<div id="shadowbottom"></div>
<div id="footer"><?php print $footer_message; ?>
	<!-- Please respect my work and keep the Miss Design text link visible in the footer. -->
	<?php if (!empty($footer)): print $footer; endif; ?><br/>Powered by <a href="http://www.drupal.org">Drupal</a>. Pink Ribbon Theme by <a href="http://www.missdesign.co.uk">Miss Design</a>.</div>
	
</div>
</body>
</html>
